import { Component, OnInit } from '@angular/core';
import { DataService } from "../data.service";
import { AuthService } from '../auth.service';
import { m } from '@angular/core/src/render3';

export class Message {
  text: string;
  from: number;
  to: number;
}

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  user: any = 1;
  message: string;
  msgs: Message[] = [];


  constructor(private data: DataService, private authService: AuthService) { }

  ngOnInit() {
    this.data.currentMessage.subscribe(u => {
      if (u != "") {
        this.user = u;
        this.authService.getMessages(u).subscribe(
          (res: any) => {
            this.msgs = res;
          }, (err: any) => {
            // this.router.navigateByUrl('/login');
          }
        );
      }
    }
    )
    this.data.newMessage.subscribe(msg => {
      console.log("New message");
      console.log(msg);
      if (msg == "") {
        return;
      }
      var m = new Message();
      var obj = JSON.parse(msg);
      m.to = obj.to;
      m.text = obj.text;
      m.from = obj.from;
      this.msgs.push(m);
    })
  }

  send() {
    this.data.sendMessage(this.message, this.user);
    console.log("This.user", this.user);
    console.log("This.message", this.message);

    var m = new Message();
    m.to = this.user;
    m.text = this.message;
    this.msgs.push(m);
    this.message = "";
  }

}
