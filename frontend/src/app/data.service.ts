import { Injectable, Injector } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from './auth.service';

// import {webSocket, WebSocketSubject} from 'rxjs/webcket';



@Injectable()
export class DataService {
  // private myWebSocket: WebSocketSubject = webSocket('ws://localhost:1337/connect/');
  private newMessageSrc = new BehaviorSubject('');
  newMessage = this.newMessageSrc.asObservable();
  private messageSource = new BehaviorSubject('');
  currentMessage = this.messageSource.asObservable();
  token: any;
  socket: any;
  
  constructor(private injector: Injector) {
    const authService = this.injector.get(AuthService);
    this.token = authService.token;
    if (this.token == null){
      return
    }
    this.socket = new WebSocket("ws://localhost:1337/connect/?token="+ this.token);
    var _this = this;
    this.socket.onmessage = function (event) {
      console.log("this.socket.onmessage", event);
      _this.newMessageSrc.next(event.data);
    };
    this.socket.onopen = function (e) { };


    this.socket.onclose = function (event) {
      if (event.wasClean) {
        alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
      } else {
        // e.g. server process killed or network down
        // event.code is usually 1006 in this case
        alert('[close] Connection died');
      }
    };
    
    this.socket.onerror = function (error) {
      // alert(`[error] ${error}`);
    };
  }

  changeMessage(message: any) {
    this.messageSource.next(message)
  }

  sendMessage(message: any, to: any) {
    this.socket.send(JSON.stringify({ "text": message, "to": to, "token": this.token }));
  }


}