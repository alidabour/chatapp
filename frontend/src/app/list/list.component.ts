import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  accountData: any;
  message: any;
  constructor(private authService: AuthService, private router: Router, private data: DataService) { }

  ngOnInit() {
    this.authService.getRooms().subscribe(
      (res: any) => {
          this.accountData = res;
      }, (err: any) => {
          // this.router.navigateByUrl('/login');
      }
  );
  this.data.currentMessage.subscribe(message => {
    this.message = message;} )
  }

  chat(userId: number){
    this.data.changeMessage(userId);
  }

}
