package main

import (
	"aut/routes"
	"log"
	"net/http"
	"os"

	"github.com/joho/godotenv"
	"github.com/rs/cors"
)

func main() {
	e := godotenv.Load()
	if e != nil {
		log.Fatal("Error loading .env file")
	}
	port := os.Getenv("PORT")
	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
		AllowedHeaders:   []string{"*"},
		Debug:            false,
	})
	http.Handle("/", c.Handler(routes.Handlers()))
	log.Printf("Server up on port '%s'", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
