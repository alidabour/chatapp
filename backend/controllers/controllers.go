package controllers

import (
	"aut/models"
	"aut/utils"
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"golang.org/x/crypto/bcrypt"
)

type ErrorResponse struct {
	Err string
}

type error interface {
	Error() string
}

var db = utils.ConnectDB()

func Login(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}
	err := json.NewDecoder(r.Body).Decode(user)
	if err != nil {
		var resp = map[string]interface{}{"status": false, "message": "Invalid request"}
		json.NewEncoder(w).Encode(resp)
		return
	}
	resp := FindOne(user.Email, user.Password)
	json.NewEncoder(w).Encode(resp)
}

func FindOne(email, password string) map[string]interface{} {
	user := &models.User{}

	if err := db.Where("Email = ?", email).First(user).Error; err != nil {
		var resp = map[string]interface{}{"status": false, "message": "Email address not found"}
		return resp
	}
	expiresAt := time.Now().Add(time.Minute * 100000).Unix()

	errf := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if errf != nil && errf == bcrypt.ErrMismatchedHashAndPassword { //Password does not match!
		var resp = map[string]interface{}{"status": false, "message": "Invalid login credentials. Please try again"}
		return resp
	}

	tk := &models.Token{
		UserID: user.ID,
		Name:   user.Name,
		Email:  user.Email,
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: expiresAt,
		},
	}

	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)

	tokenString, error := token.SignedString([]byte("secret"))
	if error != nil {
		fmt.Println(error)
	}

	var resp = map[string]interface{}{"message": "logged in"}
	resp["token"] = tokenString //Store the token in the response
	resp["user"] = user
	return resp
}

//CreateUser function -- create a new user
func CreateUser(w http.ResponseWriter, r *http.Request) {

	user := &models.User{}
	json.NewDecoder(r.Body).Decode(user)

	pass, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		fmt.Println(err)
		err := ErrorResponse{
			Err: "Password Encryption  failed",
		}
		json.NewEncoder(w).Encode(err)
	}

	user.Password = string(pass)

	createdUser := db.Create(user)
	var errMessage = createdUser.Error

	if createdUser.Error != nil {
		fmt.Println(errMessage)
	}
	json.NewEncoder(w).Encode(createdUser)
}

func FetchMessage(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	token := r.Context().Value("user").(*models.Token)
	id := params["from"]
	var msgs []models.Message
	db.Where("messages.to = ?", token.UserID).Where("messages.from = ?", id).
		Or("messages.to = ? AND messages.from = ?", id, token.UserID).Find(&msgs)
	json.NewEncoder(w).Encode(msgs)
}

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

var clients = make(map[int]*websocket.Conn)
var broadcast = make(chan models.Message)

func Chat(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println("Error,,,,", err)
		log.Fatal("Upgrade", err)
	}
	token := r.Context().Value("user").(*models.Token)
	clients[int(token.UserID)] = ws
	go readPump(ws, int(token.UserID))
	go writePump(ws)
}

func readPump(conn *websocket.Conn, user int) {
	for {
		_, message, err := conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			break
		}
		msg := &models.Message{}
		json.NewDecoder(bytes.NewReader(message)).Decode(msg)
		msg.From = user
		db.Create(msg)
		broadcast <- *msg
	}
}

func writePump(conn *websocket.Conn) {
	for {
		select {
		case message := <-broadcast:
			conn, ok := clients[message.To]
			if ok {
				err := websocket.WriteJSON(conn, message)
				if err != nil {
					log.Printf("Websocket error: %s", err)
					conn.Close()
				}
			}
		}
	}
}

func Rooms(w http.ResponseWriter, r *http.Request) {
	token := r.Context().Value("user").(*models.Token)
	var msgs []models.Message
	db.Model(&models.Message{}).
		Where("messages.to = ?", token.UserID).
		Select("Distinct(messages.from)").
		Find(&msgs)
	json.NewEncoder(w).Encode(msgs)

}
