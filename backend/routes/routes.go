package routes

import (
	"aut/controllers"
	"aut/utils/auth"

	"github.com/gorilla/mux"
)

func Handlers() *mux.Router {

	r := mux.NewRouter().StrictSlash(true)
	r.HandleFunc("/login", controllers.Login)

	chat := r.PathPrefix("").Subrouter()
	chat.Use(auth.JwtVerifyByParam)
	chat.HandleFunc("/connect/", controllers.Chat).Queries("token", "{token}")
	r.HandleFunc("/register", controllers.CreateUser).Methods("POST")
	r.HandleFunc("/login", controllers.Login).Methods("POST")

	// Auth route
	s := r.PathPrefix("").Subrouter()
	s.Use(auth.JwtVerify)
	s.HandleFunc("/message/", controllers.FetchMessage).Queries("from", "{from}").Methods("GET")
	s.HandleFunc("/rooms/", controllers.Rooms)
	return r
}
