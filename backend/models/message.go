package models

import (
	"github.com/jinzhu/gorm"
)

//Message struct declaration
type Message struct {
	gorm.Model

	From int    `json:"from"`
	To   int    `json:"to"`
	Text string `json:"text"`
}
