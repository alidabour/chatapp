#Chat APP

# Install
- Run ```docker-compose up```
- Create Admin
```
curl --request POST \
  --url http://localhost:1337/register \
  --data '{
	"email": "admin@gmail.com",
	"password": "admin123"
}'
```
- Create User
```
curl --request POST \
  --url http://localhost:1337/register \
  --data '{
	"Email": "user@gmail.com",
	"Password": "admin123"
}'
```
- Login With user on localhost/8888
- Login with Admin on localhost/8888
- Start chat from user window!


# References

- Frontend starter code https://github.com/Hitman666/AngularGoJWT
- Backend starter code https://github.com/linux08/auth
- Docker Reference https://levelup.gitconnected.com/crud-restful-api-with-go-gorm-jwt-postgres-mysql-and-testing-460a85ab7121